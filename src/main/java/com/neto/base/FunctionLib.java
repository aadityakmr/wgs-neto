package com.neto.base;

import static org.testng.Assert.assertTrue;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.ElementNotSelectableException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class FunctionLib {

	private final static WebDriverWait wait = new WebDriverWait(Base.getDriver(), Configuration.getExplicit());

	public static boolean isElementVisble(WebElement webElement) {
		try {
			wait.until(ExpectedConditions.visibilityOf(webElement));
			return true;
		} catch (ElementNotVisibleException e) {
			System.out.println("Could not find visablity of element " + webElement + " " + e.getMessage());
			return false;
		}
	}

	public static boolean isElementPresent(By alocaotr) {
		try {
			Base.getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
			WebDriverWait wait1 = new WebDriverWait(Base.getDriver(), 4);
			wait1.until(ExpectedConditions.presenceOfElementLocated(alocaotr));
			System.out.println("Element is Present");
			return true;
		} catch (NoSuchElementException e) {
			System.out.println("Could not find visablity of element " + alocaotr + " " + e.getMessage());
			return false;
		}
		catch (TimeoutException e) {
			System.out.println("Could not find visablity of element " + alocaotr + " " + e.getMessage());
			return false;
		}
		finally {
			Base.getDriver().manage().timeouts().implicitlyWait(Configuration.getImplicit(), TimeUnit.SECONDS);
		}
	}
	
	public static boolean isElementclickable(WebElement webElement) {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(webElement));
			return true;
		} catch (ElementClickInterceptedException e) {
			System.out.println("Could not find visablity of element " + webElement + " " + e.getMessage());
			return false;
		}
	}
	public static boolean isElementclickableAfterRefresh(WebElement webElement) {
		try {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(webElement)));
			return true;
		} catch (ElementClickInterceptedException e) {
			System.out.println("Could not find visablity of element " + webElement + " " + e.getMessage());
			return false;
		}
	}
	
	public static boolean isTextPresent(WebElement webElement, String textToValidate) {
		try {
			Assert.assertTrue(isElementVisble(webElement), "Failed as element is not visiable ");
			wait.until(ExpectedConditions.textToBePresentInElement(webElement, textToValidate));
			return true;
		} catch (Exception e) {
/*			System.out.println("Could not find Text " + textToValidate + " of element " + ". But found "
					+ webElement.getText() + " " + e.getMessage());
 */			System.out.println("Could not find Text "+textToValidate);
			return false;
		}
	}

	
	public static boolean isTextPresent1(WebElement webElement, String textToValidate) {
		try {
			Base.getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
			WebDriverWait wait1 = new WebDriverWait(Base.getDriver(), 4);
			Assert.assertTrue(isElementVisble(webElement), "Failed as element is not visiable ");
			wait1.until(ExpectedConditions.textToBePresentInElement(webElement, textToValidate));
			return true;
		} catch (NoSuchElementException e) {
			System.out.println("Could not find visablity of element " + webElement + " " + e.getMessage());
			return false;
		}
		catch (TimeoutException e) {
			System.out.println("Could not find visablity of element " + webElement + " " + e.getMessage());
			return false;
		
		}
		finally {
			Base.getDriver().manage().timeouts().implicitlyWait(Configuration.getImplicit(), TimeUnit.SECONDS);
		}
	}
	
	public static boolean isAllElementVisble(List<WebElement> webElements) {
		try {
			wait.until(ExpectedConditions.visibilityOfAllElements(webElements));
			return true;
		} catch (ElementNotVisibleException e) {
			System.out.println("Could not find visablity of all elements " + webElements + " " + e.getMessage());
			return false;
		}
	}

	
	
	public static boolean isTitlePresent(String title) {
		try {
			wait.until(ExpectedConditions.titleIs(title));
			return true;
		} catch (Exception e) {
			System.out.println("Could not find title as provided : " + title + " " + e.getMessage());
			return false;
		}
	}

	public static boolean selectBasedOnVisibleText(WebElement webElement, String visibleText) {
		try {
			assertTrue(isElementVisble(webElement), "Failed to view select option");
			Select select = new Select(webElement);
			select.selectByVisibleText(visibleText);
			select.getFirstSelectedOption();
			return true;

		} catch (ElementNotSelectableException e) {
			System.out.println("Failed to select due to " + e.toString());
			return false;
		}
	}
	
	public static String getSelectedOption(WebElement webElement) {
		try {
			Select select = new Select(webElement);
			return select.getFirstSelectedOption().getText();

		} catch (Exception e) {
			System.out.println("Failed to select due to" + e.toString());
			return null;
		}
	}
	
	public static boolean selectBasedOnIndex(WebElement webElement, int index) {
		try {
			Select select = new Select(webElement);
			select.selectByIndex(index);
			return true;

		} catch (ElementNotSelectableException e) {
			System.out.println("Failed to select due to " + e.toString());
			return false;
		}
	}
	
	public static boolean selectBasedOnValue(WebElement webElement, String value) {
		try {
			Select select = new Select(webElement);
			select.selectByValue(value);
			return true;

		} catch (ElementNotSelectableException e) {
			System.out.println("Failed to select due to " + e.toString());
			return false;
		}
	}

	public static boolean switchToFrame(WebElement webElement) {
		try {
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(webElement));
			return true;
		} catch (NoSuchFrameException e) {
			System.out.println("Failed to switch to frame as  " + e.toString());
			return false;
		}
	}
	
	public static String getToDayDate(String format) {
		final DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format);
		LocalDateTime now = LocalDateTime.now();
		String todayDate = dtf.format(now);
		return todayDate;
	}

	public static void toScroll(WebElement element)
	{
		JavascriptExecutor js = (JavascriptExecutor) Base.getDriver();
		js.executeScript("arguments[0].scrollIntoView();", element);
	}
	
	public static void toBottomPage() {
		JavascriptExecutor js = (JavascriptExecutor) Base.getDriver();
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}
	
	public static void sendkeys(WebElement element, String text) {
		try {
			if(isElementDisplayed(element)) {
				element.clear();
				element.sendKeys(text);
				System.out.println("Entered : "+text);
			}
		}
		catch (TimeoutException e) {
			System.out.println("Could not find visablity of element ");
		}
	}
	public static void click(WebElement element) {
		try {
			if(isElementDisplayed(element))
				element.click();
		}
	    catch (StaleElementReferenceException sere) {
	     // simply retry finding the element in the refreshed DOM
	    	element.click();
	    }
		catch (TimeoutException e) {
			System.out.println("Could not find visablity of element ");
		}
	}
	
	public static boolean isElementEnabled(WebElement element) {
		try {
			if(isElementDisplayed(element))
				element.isEnabled();
				System.out.println(element+ "is enabled");
			return true;
		} catch (ElementNotVisibleException e) {
			System.out.println("Could not find visablity of all elements " + element + " " + e.getMessage());
			return false;
		}
	}
	
	
	public static boolean isElementDisplayed(WebElement element) {
		try {
			Base.getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
			WebDriverWait wait = new WebDriverWait(Base.getDriver(), 10);
			wait.until(ExpectedConditions.visibilityOf(element));
			System.out.println(element+" is displaying");
			return true;
		} catch (Exception e) {
			System.out.println("Could not find visablity of element" + element + " " + e.getMessage());
			return false;
		}
		finally {
			Base.getDriver().manage().timeouts().implicitlyWait(Configuration.getImplicit(), TimeUnit.SECONDS);
		}
	}
	
	
	public static boolean isElementNotVisible(WebElement element) {
		boolean check=true;
		try {
			Base.getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
			while (check) {
				if(!isElementDisplayed(element))
					break;
			}
			System.out.println("checking element: "+check);
		} catch (Exception e) {
			check=false;
		} finally {
			Base.getDriver().manage().timeouts().implicitlyWait(Configuration.getImplicit(), TimeUnit.SECONDS);
		}
		return check;
	}
	
	public static void expectconditions(WebElement element, String command) {
		try {
			Base.getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
			WebDriverWait wait = new WebDriverWait(Base.getDriver(), 10);
			
			switch (command) {
			case "switchtoframe":
				Base.getDriver().switchTo().defaultContent();
				wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(element));
				break;
				
			case "switchtoframeinside":
				wait.until(ExpectedConditions.jsReturnsValue("return document.readyState=='complete'"));
				wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(element));
				break;
			}
		} catch (Exception e) {
			System.out.println("Could not find visablity of element" + element + " " + e.getMessage());
		}
		finally {
			Base.getDriver().manage().timeouts().implicitlyWait(Configuration.getImplicit(), TimeUnit.SECONDS);
		}
	}
}
