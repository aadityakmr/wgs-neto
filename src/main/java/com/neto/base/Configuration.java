package com.neto.base;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Configuration {
	private static final Properties prop = new Properties();
	private static final String CONFIGFILE = "Resources/config.properties";
	
	static {
		try {
			prop.load(new FileInputStream(CONFIGFILE));
		} catch (IOException e) {
			System.out.println(e.toString());
		}
	}

	private Configuration() {
	    throw new IllegalStateException("Utility class");
	}
	
	
	private static final String url_uat = getProperties("uat");
	private static final String url_itbau = getProperties("itbau");
	private static final String env = getProperties("env");
	
	private final static String lo_LoginUserRetail=getProperties("rlo_userid");
	private final static String lo_LoginUserCARD=getProperties("clo_userid");
	private final static String lo_LoginPassword=getProperties("lo_pwd");
	
	private static final  String browser = getProperties("browser");
	private static final  String appName = getProperties("AppName");
	private static final  int implicit = Integer.parseInt(getProperties("implicit"));
	private static final  int pageLoad = Integer.parseInt(getProperties("pageLoad"));
	private static final  int explicit = Integer.parseInt(getProperties("explicit"));
	private static final  int script = Integer.parseInt(getProperties("script"));
	
	
	public static String getbrowser() {
		return browser;
	}
	
	public static int getScript() {
		return script;
	}

	private final static String getProperties(String akey) {
		return prop.getProperty(akey);
	}

	public static int getImplicit() {
		return implicit;
	}

	public static int getPageload() {
		return pageLoad;
	}

	public static int getExplicit() {
		return explicit;
	}
	
	public static String getAppName() {
		return appName;
	}

	public static String getEnv() {
		return env;
	}

	public static String getUrlUat() {
		return url_uat;
	}

	public static String getUrlItbau() {
		return url_itbau;
	}

	public static String getLOLoginUserRetail() {
		return lo_LoginUserRetail;
	}

	public static String getLOLoginUserCARD() {
		return lo_LoginUserCARD;
	}

	public static String getLOLoginPassword() {
		return lo_LoginPassword;
	}
	
}

	



