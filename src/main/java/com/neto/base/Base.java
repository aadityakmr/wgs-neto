package com.neto.base;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class Base {
	public static WebDriver driver;
	
	@BeforeTest(alwaysRun = true)
	@Parameters("mybrowser")
	public void initializeDriver(String mybrowser) {
		setDriverPath(mybrowser);
		initialization(mybrowser);
	}

	public void initialization(String browser) {
		if (browser.equalsIgnoreCase("chrome"))
			driver = new ChromeDriver();
		else if (browser.equalsIgnoreCase("firefox"))
			driver = new FirefoxDriver();
		else if (browser.equalsIgnoreCase("ie")) {
			driver = new InternetExplorerDriver();
		}
		settingBrowser();
		if(Configuration.getEnv().equalsIgnoreCase("itbau"))
		{
			driver.get(Configuration.getUrlItbau());
		}
		if(Configuration.getEnv().equalsIgnoreCase("uat"))
		{
			driver.get(Configuration.getUrlUat());
		}
	}

//	@AfterMethod(alwaysRun = true)
//	public static void closeBrowser() throws EmailException {
////		driver.close();
//		SentEmailNotification.emailSent1();
//	}

//	@AfterSuite(alwaysRun = true)
//	public static void closeWebDriver() {
//		if (driver != null & !browser.equalsIgnoreCase("firefox"))
//			driver.quit();
//	}

	
	private static void settingBrowser() {
		getDriver().manage().timeouts().implicitlyWait(Configuration.getImplicit(), TimeUnit.SECONDS);
		getDriver().manage().timeouts().pageLoadTimeout(Configuration.getPageload(), TimeUnit.SECONDS);
		getDriver().manage().timeouts().setScriptTimeout(Configuration.getScript(), TimeUnit.SECONDS);
		getDriver().manage().deleteAllCookies();
		getDriver().manage().window().maximize();
	}

	private void setDriverPath(String browser) {
		if (browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "Resources/chromedriver");
		}
		else if (browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver","Resources/geckodriver.exe");
		}
		else if (browser.equalsIgnoreCase("IE")) {
			System.setProperty("webdriver.ie.driver","Resources/IEDriverServer.exe");
		}
		else {
			System.exit(1);
		}
	}
	
	public static WebDriver getDriver() {
		return driver;
	}
}

