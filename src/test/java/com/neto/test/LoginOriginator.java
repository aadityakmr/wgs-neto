package com.neto.test;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.neto.base.Base;
import com.neto.base.FunctionLib;


public class LoginOriginator extends Base {
	
	@Test
	public void LoginMLO() throws InterruptedException {
		System.out.println("Open the site");
		
		FunctionLib.expectconditions(Base.getDriver().findElement(By.id("GFXLogonFrame")), "switchtoframe");
		Base.getDriver().findElement(By.id("username")).sendKeys("$originator@demo");
		Base.getDriver().findElement(By.id("password")).sendKeys("Gallagher01!");
		Base.getDriver().findElement(By.id("Submit1")).click();
		Thread.sleep(1000);
		
		FunctionLib.expectconditions(Base.getDriver().findElement(By.id("grpselect")), "switchtoframeinside");
		FunctionLib.selectBasedOnIndex(Base.getDriver().findElement(By.id("selectGroup")), 1);
		FunctionLib.click(Base.getDriver().findElement(By.id("btnOk")));
	}
	
	
//	@Test(dependsOnMethods="LoginMLO")
	public void importLoan() {
		FunctionLib.expectconditions(Base.getDriver().findElement(By.id("GFXLoanSelectorFrame")), "switchtoframe");
		Base.getDriver().findElement(By.xpath("//*[@title='Import/Export']")).click();
		Base.getDriver().findElement(By.xpath("//li/input[@type='button' and @title='Import Loan']")).click();
		FunctionLib.expectconditions(Base.getDriver().findElement(By.xpath("//iframe[@src='WGS_UserDashboard_Import.htm']")), "switchtoframe");
	}
}
